package com.example.larissanavarro.reactivetest.activities.examples;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.larissanavarro.reactivetest.R;
import com.example.larissanavarro.reactivetest.utils.SimpleStringAdapter;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;

public class Example1Activity extends AppCompatActivity {

    private static final String TAG = "InLoco:Example1Activity";

    RecyclerView mColorListView;
    SimpleStringAdapter mSimpleStringAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example_1);

        configureLayout();
        createObservable();
    }

    private void createObservable() {

        // Observables are primarily defined by their behavior upon subscription
        Observable<List<String>> listObservable = Observable.just(getColorList());
        listObservable.subscribe(new Observer<List<String>>() {
            @Override
            public void onCompleted() {
                Log.i(TAG, "Observable stream complete");
            }

            @Override
            public void onError(final Throwable e) {
                Log.e(TAG, "Observable error: " + e.getMessage(), e);
            }

            @Override
            public void onNext(final List<String> strings) {
                mSimpleStringAdapter.setStrings(strings);
            }
        });
    }

    private void configureLayout() {
        setContentView(R.layout.activity_example_1);
        mColorListView = (RecyclerView) findViewById(R.id.color_list);
        mColorListView.setLayoutManager(new LinearLayoutManager(this));
        mSimpleStringAdapter = new SimpleStringAdapter(this);
        mColorListView.setAdapter(mSimpleStringAdapter);
    }

    private static List<String> getColorList() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("blue");
        colors.add("green");
        colors.add("red");
        colors.add("chartreuse");
        colors.add("Van Dyke Brown");
        return colors;
    }
}
