package com.example.larissanavarro.reactivetest.activities.examples;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.larissanavarro.reactivetest.R;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;

public class Example7Activity extends AppCompatActivity {

    private static final String TAG = "InLocoExample7Activity";

    private static final String DISPLAY_TEXT = "Double clicks: %d";

    private TextView mClickCounter;
    private Button mClickButton;

    private Observable<Void> mClickObservable;
    private Subscription mDoubleClickSubscription;

    private int mClickCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example7);

        mClickCounter = (TextView) findViewById(R.id.counter);
        mClickButton = (Button) findViewById(R.id.button_double_click);

        createObservable();
        subscribe();
    }

    private void subscribe() {
        mDoubleClickSubscription = mClickObservable.buffer(500, TimeUnit.MILLISECONDS)
                .map(new Func1<List<Void>, Integer>() {
                    @Override
                    public Integer call(final List<Void> voids) {
                        return voids.size();
                    }
                }).filter(new Func1<Integer, Boolean>() {
                    @Override
                    public Boolean call(final Integer integer) {
                        return integer >= 2;
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "onCompleted called");
                    }

                    @Override
                    public void onError(final Throwable e) {
                        Log.i(TAG, "onError called");
                    }

                    @Override
                    public void onNext(final Integer integer) {
                        updateDoubleClickCount();
                    }
                });
    }

    private void updateDoubleClickCount() {
        String message = String.format(Locale.US, DISPLAY_TEXT, ++mClickCount);
        mClickCounter.setText(message);
    }

    private void createObservable() {
        mClickObservable = Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(final Subscriber<? super Void> subscriber) {
                if (!subscriber.isUnsubscribed() && mClickButton != null) {
                    mClickButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            subscriber.onNext(null);
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (mDoubleClickSubscription != null && !mDoubleClickSubscription.isUnsubscribed()) {
            mDoubleClickSubscription.unsubscribe();
        }
        super.onDestroy();
    }
}
