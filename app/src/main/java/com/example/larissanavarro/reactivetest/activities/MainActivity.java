package com.example.larissanavarro.reactivetest.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.larissanavarro.reactivetest.activities.examples.Example1Activity;
import com.example.larissanavarro.reactivetest.activities.examples.Example2Activity;
import com.example.larissanavarro.reactivetest.R;
import com.example.larissanavarro.reactivetest.activities.examples.Example3Activity;
import com.example.larissanavarro.reactivetest.activities.examples.Example4Activity;
import com.example.larissanavarro.reactivetest.activities.examples.Example5Activity;
import com.example.larissanavarro.reactivetest.activities.examples.Example6Activity;
import com.example.larissanavarro.reactivetest.activities.examples.Example7Activity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setButtons();
    }

    private void setButtons() {

        // Observable from List
        Button activity1Button = (Button) findViewById(R.id.activity_1);
        if (activity1Button != null) {
            activity1Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    Intent intent = new Intent();
                    intent.setClass(getBaseContext(), Example1Activity.class);

                    startActivity(intent);
                }
            });
        }

        // Asynchronous Loading
        Button activity2Button = (Button) findViewById(R.id.activity_2);
        if (activity2Button != null) {
            activity2Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    Intent intent = new Intent();
                    intent.setClass(getBaseContext(), Example2Activity.class);

                    startActivity(intent);
                }
            });
        }

        // Singles (with error)
        Button activity3Button = (Button) findViewById(R.id.activity_3);
        if (activity3Button != null) {
            activity3Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    Intent intent = new Intent();
                    intent.setClass(getBaseContext(), Example3Activity.class);

                    startActivity(intent);
                }
            });
        }

        // Subjects
        Button activity4Button = (Button) findViewById(R.id.activity_4);
        if (activity4Button != null) {
            activity4Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    Intent intent = new Intent();
                    intent.setClass(getBaseContext(), Example4Activity.class);

                    startActivity(intent);
                }
            });
        }

        // Mapping
        Button activity5Button = (Button) findViewById(R.id.activity_5);
        if (activity5Button != null) {
            activity5Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    Intent intent = new Intent();
                    intent.setClass(getBaseContext(), Example5Activity.class);

                    startActivity(intent);
                }
            });
        }

        // All Together
        Button activity6Button = (Button) findViewById(R.id.activity_6);
        if (activity6Button != null) {
            activity6Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    Intent intent = new Intent();
                    intent.setClass(getBaseContext(), Example6Activity.class);

                    startActivity(intent);
                }
            });
        }

        // All Together
        Button activity7Button = (Button) findViewById(R.id.activity_7);
        if (activity7Button != null) {
            activity7Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    Intent intent = new Intent();
                    intent.setClass(getBaseContext(), Example7Activity.class);

                    startActivity(intent);
                }
            });
        }
    }
}
