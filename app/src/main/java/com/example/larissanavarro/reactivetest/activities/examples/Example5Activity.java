package com.example.larissanavarro.reactivetest.activities.examples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.larissanavarro.reactivetest.R;

import rx.Single;
import rx.SingleSubscriber;
import rx.functions.Func1;

public class Example5Activity extends AppCompatActivity {

    private TextView mValueDisplay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example_5);

        configureLayout();
        createSingle();
    }

    private void createSingle() {
        Single.just(4).map(new Func1<Integer, String>() {
            @Override
            public String call(final Integer integer) {
                return String.valueOf(integer);
            }
        }).subscribe(new SingleSubscriber<String>() {
            @Override
            public void onSuccess(final String value) {
                mValueDisplay.setText(value);
            }

            @Override
            public void onError(final Throwable error) {}
        });
    }

    private void configureLayout() {
        setContentView(R.layout.activity_example_5);
        mValueDisplay = (TextView) findViewById(R.id.value_display);
    }
}
